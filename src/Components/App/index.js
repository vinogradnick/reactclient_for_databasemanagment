import React, { Component } from 'react';
import OpenConnection from '../../Containers/OpenConnection'
import { Layout, Menu, Icon,notification } from 'antd';
import './App.css'
import TreeView from "../TreeView";
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;





class App extends Component {
    state = {
        collapsed: false,
    };
    onCollapse = (collapsed) => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

  render() {
    return (
        <Layout>
            <Sider
                trigger={null}
                collapsible
                collapsed={this.state.collapsed}
                onCollapse={this.onCollapse}

            >
                <div className="logo" />
                <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%', borderRight: 0 }}
                >
                    <SubMenu key="sub1"title={<span><Icon type="database" />PostgreSQL Server </span>}>

                            <TreeView/>
                    </SubMenu>
                    <SubMenu key="sub2" title={<span><Icon type="laptop" />User settings</span>}>
                        <Menu.Item key="5">Privileges</Menu.Item>

                    </SubMenu>
                    <SubMenu key="sub3" title={<span><Icon type="notification" />About HSE Project PostgreSQL Admin</span>}>
                        <Menu.Item key="9"><span><Icon type="github"/>Github Repository</span></Menu.Item>

                    </SubMenu>
                </Menu>
            </Sider>
            <Layout>
                <Header style={{ background: '#fff', paddingLeft:20 }}>
                    <Icon
                        className="trigger"
                        type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                        onClick={this.onCollapse}
                    />
                </Header>
                <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>

                </Content>
            </Layout>
        </Layout>
    );
  }
}

export default App;
