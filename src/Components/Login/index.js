import React,{Component} from 'react';

import {Redirect} from 'react-router-dom'
import LoginForm from "../../Containers/LoginFom"
import {notification} from 'antd';

const openNotificationWithIcon = (type) => {
    switch (type) {
        case "success":
            notification[type]({
                message: 'Request Successful',
                description: 'Your connected to PostreSQL database',
            });
           break;
        case "error":
            notification[type]({
                message: 'Request Error',
                description: 'Connection to PostreSQL database invalid',
            });
            break;

    }

 };


export default  class Login extends  Component{
    state ={
        connected:false
    }

    authConnection = (connectedStatus)=>{
        console.log("main login form")
        this.setState({connected:connectedStatus});
        if(connectedStatus) openNotificationWithIcon("success"); else openNotificationWithIcon("error");
    }

    render() {

        return this.state.connected ? <Redirect to="/home" from="/"/> : (<LoginForm authStatus={this.authConnection}/>

        );

    }
}

