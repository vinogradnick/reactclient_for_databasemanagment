import React, {Component} from 'react';
import {Button, Form, Icon, Input, Layout} from "antd";
import Logo from "./logo.svg";
const FormItem = Form.Item;
const { Header, Content, Footer } = Layout;

export default class LoginForm extends  Component {
//todo need to resolve problem with Double AUTH CLICKS | Maybe it is problem in {state}

    state ={
        connected:false,
        name:'',
        password:''
    }

    onChangeUsername = (e)=>{
        this.setState({name:e.target.value})

    }
    onChangePassword = (e)=>{
        this.setState({password:e.target.value})
    }
    login= (e)=>{
        const user ={
            name:this.state.name,
            password: this.state.password
        }
        this.props.onAuthUser(user);

        this.props.authStatus(this.props.authStatus);
    }


    render() {
        return (
            <div>
                <Layout className="layout">

                    <Content style={{padding: 50}}>


                        <div style={{background: '#fff', padding: 24, minHeight: 280}}>
                            <div style={{textAlign: 'center'}}>
                                <img src={Logo} alt="" width={200}/>
                                <h1>PostgreSQL Database <br/>Login Page</h1>

                            </div>
                            <Form  className="login-form"
                                  style={{align: 'center', padding: 100}}>
                                <FormItem>
                                    <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                           placeholder="Username" onChange={this.onChangeUsername}/>
                                </FormItem>
                                <FormItem>
                                    <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} onChange={this.onChangePassword} type="password"
                                           placeholder="Password"/></FormItem>
                                <FormItem>
                                    <Button type="primary" htmlType="submit" onClick={this.login} className="login-form-button">
                                        Log in
                                    </Button>
                                </FormItem>
                            </Form>
                        </div>
                    </Content>
                    <Footer style={{textAlign: 'center'}}>
                        HSE student project ©2018 Created by Vinograd
                    </Footer>
                </Layout>

            </div>
        );
    }

}