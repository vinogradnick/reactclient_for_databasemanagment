import React, {Component} from 'react';
import PropTypes from 'prop-types'

class OpenConnection extends Component {



    handleClick =()=>{
        this.props.onOpenConnection(this.state);
        console.log(this.props.database)

    };

    render() {
        return (
            <div>
                <button onClick={this.handleClick} >Кнопка</button>

            </div>

        );
    }
}
OpenConnection.PropType={
    database:PropTypes.object.isRequired
}
export default OpenConnection;