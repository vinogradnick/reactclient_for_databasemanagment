import React, {Component} from 'react';
import { Tree } from 'antd';
const TreeNode = Tree.TreeNode;



export default class TreeView extends Component{

    state = {
        treeData: [
            { title: 'Postgresql', key: '0' },

        ],
    }

    onLoadData = (treeNode) => {
        return new Promise((resolve) => {
            if (treeNode.props.children) {
                resolve();
                return;
            }
            setTimeout(() => {
                treeNode.props.dataRef.children = [
                    { title: 'Hello', key: `${treeNode.props.eventKey}-0` },
                    { title: 'Child Node', key: `${treeNode.props.eventKey}-1` },
                ];
                this.setState({
                    treeData: [...this.state.treeData],
                });
                resolve();
            }, 1000);
        });
    }
    renderTreeNodes = (data) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} dataRef={item} />;
        });
    }

    render() {
        return (
            <Tree loadData={this.onLoadData} style={{PaddingLeft:20}}>
                {this.renderTreeNodes(this.state.treeData)}
            </Tree>
        );
    }
}