import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import registerServiceWorker from './registerServiceWorker';
import {createStore, applyMiddleware} from  'redux';
import RootReducer from './Reducers'
import 'antd/dist/antd.css';
import {fetchDataConnection} from "./Actions";
import Provider from "react-redux/es/components/Provider";
import {BrowserRouter as Router, Route} from "react-router-dom";
import App from "./Components/App";
import Login from  './Components/Login'

/**
 * Initialize Redux Store and Load Dispatcher Functions
 * @type {Store<*, Action> & {dispatch: any}}
 */
const store = createStore(RootReducer,applyMiddleware(thunk));


/**
 *  Render for Entry point of Application
 */
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <div>
                <Route path="/" component={Login} />
                <Route path="/home" component={App}/>
            </div>
        </Router>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
