import {AUTH_USER_FAILED, AUTH_USER_SUCCESSFUL, OPEN_CONNECTION} from "../Actions/constants";

const initialState= {
    connected:false,
    databaseInfo:{}
}

export default function connectionReducer(state=initialState,action) {
    switch (action.type) {
        case OPEN_CONNECTION:

            console.log("this is action of rootReducer");
            return Object.assign({},state,{
                databaseInfo: action.connection.data
            })
        case AUTH_USER_SUCCESSFUL:
            console.log("user is auth");
            return Object.assign({},state,{
                connected:true
            })
        case AUTH_USER_FAILED:
            console.log("fail is auth");
            //todo if sever was started then change to false value
            return  Object.assign({},state,{
                connected: true
            })
        default:
            return state;

    }

}