import { combineReducers} from 'redux';
import connection from './connectionReducer'

/**
 * Combine reducers can help split your reducing function into separate functions, each managing independent parts of the state.
 */
export default combineReducers({
    connection:connection,
    auth:connection,
})


