import { connect } from 'react-redux';
import {authUser} from "../Actions/auth";
import  LoginForm from '../Components/Login/LoginForm'

/**
 *
 * @param state Current state of application
 * @returns {{authStatus: boolean}} status user conenction
 */
const mapStateToProps = (state)=>{
    console.log("map state changed");
    return {
        authStatus:state.auth.connected
    }
}
/**
 * Function send dispatchers to props for using in Component
 * @param dispatch
 * @returns {{onAuthUser: onAuthUser}} function for authenticate user
 */
const mapDispatchToProps = (dispatch)=>{
    console.log("this is event");
    return{
        onAuthUser: (user)=>{
            dispatch(authUser(user));
        }
    }
}
/**
 * Turn Component and mapStateFunctions in one Container and export to Login component
 */
export  default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm)