import { connect } from 'react-redux';
import {fetchDataConnection} from "../Actions";
import OpenConnection from '../Components/OpenConnection'


const mapStateToProps = (state)=> {
    console.log("state chaged")
    console.log(state);

    return {
        database: state.connection.databaseInfo
    }
}

const mapDispatchToProps = (dispatch)=> {
    return{
        onOpenConnection:connect=>{ dispatch(fetchDataConnection())}
    };

};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OpenConnection);