import {AUTH_USER_FAILED, AUTH_USER_SUCCESSFUL, OPEN_CONNECTION} from "./constants";
import axios from  'axios';

//static api url for connection
export const staticURL = 'http://localhost:3000';
// OpenConnection for database
export  function OpenConnection(connection) {
    return {
        type: OPEN_CONNECTION,
        connection
    };

}

//Get information from database about [Connection] also get Information Schema Database
export  function fetchDataConnection() {
    return (dispatch)=>{
        return axios.get(staticURL)
            .then(response=>{
                console.log(response)
            dispatch(OpenConnection(response))//send data in store
        })
            .catch(err=>{throw(err)});
    };

};
