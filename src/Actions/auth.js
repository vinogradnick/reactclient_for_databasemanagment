import {AUTH_USER_FAILED, AUTH_USER_SUCCESSFUL} from "./constants";
import {AxiosInstance as axios} from "axios";
import {staticURL} from "./index";

/**
 * @param authData involve {username,password} params  of user
 * */
export function authUserSuccess(authData) {
    return {
        type:AUTH_USER_SUCCESSFUL,
        authData
    }

}
/**
 * @param authData
 * */
export function authUserFail(authData){
    return{
        type:AUTH_USER_FAILED,
        authData
    }
}
//todo: make [axios request]QUERY for got json data from nodejs server
/**
 * This function try to connecto database and authenticate user
 * @param user userdata of user for connection to database
 * @returns {function(*): Promise<AxiosResponse<any> | never>}
 */
export function authUser(user){
    return (dispatch)=>{
        console.log(user)
        return axios.post(staticURL+"/connect",{user:user.name,password:user.password})
            .then(response => dispatch(authUserSuccess(response.data)))
            .catch(err=>{dispatch(authUserFail(err.data))});
    }
}